const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {}

function writeToJsonFile(params) {

    splitData(params)
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
        console.log("All done!");
      });

}

function splitData(params) {
    output.emails = [];
    for (let index = 0; index < params.length; index++) {
        output.emails.push(reverseString(params[index]));
    }
    
    console.log(output.emails);
}

function reverseString(params) {
    for (let index = 0; index < params.length; index++) {
        var reverseArray = params.split("").reverse().join("")
        rngString = randomstring.generate(5);
        reverseArray = reverseArray + rngString + "@gmail.com"
        return reverseArray;
    }
}

jsonfile.readFile(inputFile)
  .then(obj => writeToJsonFile(obj.names))
  .catch(error => console.error(error))