const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];

console.log("creating looping function")
const getAcronym = function() {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL+acronym).then(response => {
    console.log("got data for acronym", acronym);
    console.log("add returned data to definitions array");
    output.definitions.push(response.body);
    var json = JSON.stringify(output);
      json = json.replace(/\\n/g, '');
      output = JSON.parse(json)
    console.log(output);
    if (output.definitions.length < 10) {
      console.log("calling looping function again");
      getAcronym();
    }
    else {
      
      console.log("saving output file formatted with 2 space indenting");
      jsonfile.writeFile(outputFile, output, { spaces: 2, finalEOL: false }, function(err) {
        console.log("All done!");
      });
    }
  }).catch(err => {
    console.log(err)
  })
}

console.log("calling looping function");
getAcronym();